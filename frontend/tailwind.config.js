const colors = require('tailwindcss/colors');
/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            black: colors.black,
            white: colors.white,
            rose: colors.rose,
            pink: colors.pink,
            fuchsia: colors.fuchsia,
            purple: colors.purple,
            violet: colors.violet,
            indigo: colors.indigo,
            blue: colors.blue,
            sky: colors.sky,  
            cyan: colors.cyan,
            teal: colors.teal,
            emerald: colors.emerald,
            green: colors.green,
            lime: colors.lime,
            yellow: colors.yellow,
            amber: colors.amber,
            orange: colors.orange,
            red: colors.red,
            slate: colors.slate,
            zinc: colors.zinc,
            gray: colors.gray,
            neutral: colors.slate,
            stone: colors.stone,
        },
        extend: {
            keyframes: {
                underline: {
			  		'0%': { borderBottom: '0%' },
			  		'100%': { borderBottom: '100%' },
                }
		  	},
            animation: {
                underline: 'underline 1s ease-in-out',
            }
		  
        },
    },
    plugins: [
        require('@tailwindcss/forms'),
        require("daisyui")
    ],
}
