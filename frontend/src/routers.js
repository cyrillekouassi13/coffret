import { createRouter, createWebHistory } from 'vue-router'
import store from './storage'
import Login from './pages/auth/Login.vue'
import Register from './pages/auth/Register.vue'
import Home from './pages/Home.vue'
import PasswordReset from './pages/auth/PasswordReset.vue'
import PasswordResetConfirm from './pages/auth/PasswordResetConfirm.vue'
import { getUrlLanguage } from './routing'
import { saveLocale } from './utils'

// reward
import RewardList from './pages/reward/RewardList.vue'
import RewardCreate from './pages/reward/RewardCreate.vue'

export const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
        meta: {requiresAuth: true}
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
    },
    {
        path: "/register",
        name: "Register",
        component: Register,
    },
    {
        path: "/settings/password/reset",
        name: "password_reset",
        component: PasswordReset,
    },
    {
        path: "/settings/password/reset/confirm/:uid/:token",
        name: "password_reset_confirm",
        component: PasswordResetConfirm,
        props: true
    },

    // reward
    {
        path: "/rewards",
        name: "reward_list",
        component: RewardList,
        meta: {requiresAuth: true}
    },
    {
        path: "/reward/create",
        name: "reward_create",
        component: RewardCreate,
        meta: {requiresAuth: true}
    },
    {
        path: "/reward/:id",
        name: "reward_detail",
        component: RewardCreate,
        meta: {requiresAuth: true},
        props: true
    }
]

const router = createRouter({
    history: createWebHistory(""),
    routes,
})

router.beforeResolve((to, from, next) => {
    /* if (from.name) {
        console.log('in')
        NProgress.start()
    } */
    next()
})
  
router.beforeEach(async(to, from) => {
    const language = getUrlLanguage(to.path);

    if (language) {
        saveLocale(language)
        
        return to.fullPath.slice(language.length + 1)
    }

    if (to.meta.requiresAuth) {
        await store.dispatch('auth/refresh')

        if (!store.state.auth.isAuth) {
            return {
                path: window.Routing.getRoutePath('Login'),
                query: { redirect: to.fullPath },
            }
        }
    }
})

router.afterEach((to, from) => {
    //NProgress.done()
}) 
  

export default router;