import { LANGUAGE_COOKIE_LIFE_DAYS, LANGUAGE_COOKIE_NAME } from "./constants";
import { routes } from "./routers"

export const getRoutePath = (name) => (
    routes
        .find(route => route.name == name)
        .path
)

export const getCookie = (name) => {
    const cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();
        if (cookie.startsWith(name + '=')) {
            return cookie.substring(name.length + 1);
        }
    }
    return null;
}


export const setCookie = (name, value, days) => {
    const expires = new Date();
    expires.setTime(expires.getTime() + days * 24 * 60 * 60 * 1000);
    document.cookie = name + '=' + value + ';expires=' + expires.toUTCString() + ';path=/';
}

export const getLocale = () => (
    getCookie(LANGUAGE_COOKIE_NAME)
)

export const saveLocale = (locale) => {
    setCookie(
        LANGUAGE_COOKIE_NAME,
        locale,
        LANGUAGE_COOKIE_LIFE_DAYS,
    )
}

export function toMoney(val, precision = Constants.GENERAL.precision, fixedPoint = true) {
    Decimal.set({rounding: Decimal.ROUND_DOWN, toExpNeg: -20});

    val = new Decimal(val);
    precision = val.lessThan(1 / Math.pow(10, precision)) ? 0 : precision;
    val = val.toDP(precision);

    return fixedPoint
        ? val.toString()
        : val;
}

export function formatMoney(str) {
    str = str ? str.toString() : '';
    str = str.split(/ (.+)/);
    let additional = str[1] || '';
    str = str[0];
    let regx = /(\d{1,3})(\d{3}(?:,|$))/;
    let currStr;

    do {
        currStr = (currStr || str.split(`.`)[0]).replace( regx, `$1,$2`);
    } while (currStr.match(regx));

    let res = ( str.split(`.`)[1] ) ?
        currStr.concat(`.`, str.split(`.`)[1]) :
        currStr;

    return `${res.replace(/,/g, ' ')} ${additional}`.trim();
}