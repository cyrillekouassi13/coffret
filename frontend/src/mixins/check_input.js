export default {
    data() {
        return {
            prevInputValues: {},
        };
    },
    methods: {
        checkInput(precision = 2, digits = 6) {
            let selectionStart = event.target.selectionStart;
            let selectionEnd = event.target.selectionEnd;
            let amount = event.target.value;

            let input = event instanceof ClipboardEvent
                ? event.clipboardData.getData('text')
                : String.fromCharCode(!event.charCode ? event.which : event.charCode);

            const currentValue = amount.slice(0, selectionStart) + input + amount.slice(selectionEnd);

            if (!this.checkInputValue(currentValue, precision, digits)) {
                event.preventDefault();

                if (!event.cancelable && event.target.id && currentValue != '') {
                    event.target.value = this.prevInputValues[event.target.id] || '';
                }

                return false;
            }

            if (event.target.id) {
                this.prevInputValues[event.target.id] = currentValue;
            }

            return true;
        },
        checkInputValue(value, precision = 2, digits = 6) {
            const regex = new RegExp(
                `^\\d*(\\.\\d{0,${precision}})?\\.?\\d{0,${digits}}$|^\\d*(\\.\\d{0,${precision}})?e\\d{0,${digits}}'?$`
            );

            return regex.test(value);
        },
        checkInputDot() {
            if ('.' === event.target.value) {
                event.target.value = '0.';
            }
        },
    },
};
