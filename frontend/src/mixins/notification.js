import { POSITION, TYPE, useToast } from "vue-toastification";

export default {
    data() {
        return {
            notifyDuration: 10000,
            notifyPosition: POSITION.TOP_CENTER
        };
    },
    methods: {
        notifyError: function(message, duration = this.notifyDuration) {
            return this.sendNotification(message, TYPE.ERROR, duration);
        },
        notifyInfo: function(message, duration = this.notifyDuration) {
            return this.sendNotification(message, TYPE.INFO, duration);
        },
        notifySuccess: function(message, duration = this.notifyDuration) {
            return this.sendNotification(message, TYPE.SUCCESS, duration);
        },
        notifyWarning: function(message, duration = this.notifyDuration) {
            return this.sendNotification(message, TYPE.WARNING, duration);
        },
        sendNotification: function(message, type, duration = this.notifyDuration ) {
            const toast = useToast();

            return toast(
                message,
                {
                    type,
                    icon: true,
                    duration,
                    position: this.notifyPosition
                }
            );
        },
    },
};
