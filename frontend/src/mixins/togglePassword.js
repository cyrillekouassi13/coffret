import {library} from '@fortawesome/fontawesome-svg-core';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

library.add(faEye, faEyeSlash);

export default {
    data () {
        return {
            isPassVisible: false,
            eyeIcon: 'fa-solid fa-eye',
            inputType: 'password',
        }
    },
    methods: {
        togglePassword: function() {
            this.isPassVisible ? this.hidePassword() : this.showPassword();
        },
        showPassword: function() {
            this.isPassVisible = true;
        },
        hidePassword: function() {
            this.eyeIcon = 'fa-solid fa-eye-slash';
            this.isPassVisible = false;
        },
    },
    watch: {
        isPassVisible (value) {
            if(value) {
                this.inputType = 'text';
                this.eyeIcon = 'fa-solid fa-eye-slash';
            } else {
                this.eyeIcon = 'fa-solid fa-eye';
                this.inputType = 'password';
            }
        }
    }
};
