import { createApp } from 'vue';
import './style.css';
import AxiosPlugin from './axios';
import App from './App.vue';
import router from './routers';
import store from './storage';
import routing from './routing';
import Toast from "vue-toastification";
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import { createI18n } from 'vue-i18n'
import { messages } from './i18n';
import FlagIcon from 'vue-flag-icon';
import { DEFAULT_LANGUAGE} from './constants';
// Import the CSS for toastification
import "vue-toastification/dist/index.css";

const i18n = createI18n({
    messages,
    locale: 
        store.state.locale.currentLocale
        ?? DEFAULT_LANGUAGE,
    fallbackLocale: 'en'
})

const app = createApp(App);

app.use(AxiosPlugin);
app.use(i18n);
app.use(routing);
app.use(router);
app.use(store);
app.use(FlagIcon);
app.component('font-awesome-icon', FontAwesomeIcon)

app.use(Toast, {
    transition: "Vue-Toastification__bounce",
    maxToasts: 10,
    newestOnTop: true    
});

app.mount('#app');
