export const ENDPOINTS = [
    {
        name: 'login',
        path: 'auth/login/',
    },{
        name: 'register',
        path: 'auth/registration/',
    },{
        name: 'token_refresh',
        path: 'auth/token/refresh/',
    },{
        name: 'logout',
        path: 'auth/logout/',
    },{
        name: 'password_reset',
        path: 'auth/password/reset/',
    },{
        name: 'password_reset_confirm',
        path: 'auth/password/reset/confirm/',
    },{
        name: 'user',
        path: 'auth/user/',
    },{
        name: 'rewards',
        path: 'api/planner/reward/',
    },{
        name: 'reward',
        path: 'api/planner/reward/{id}/',
    },
]

export const STATUS = {
    HTTP_BAD_REQUEST: 400,
};

export const DEFAULT_LANGUAGE = 'fr';

export const LANGUAGES = {
    en: { flag: 'us', title: 'english' },
    fr: { flag: 'fr', title: 'french' },
    es: { flag: 'es', title: 'spanish' }
}

export const HOME = '/'

export const LANGUAGE_COOKIE_NAME = 'django_language'
export const LANGUAGE_COOKIE_LIFE_DAYS = 5;

export const REWARD_CONTENT_TEXT_MIN = 20;
export const REWARD_CONTENT_AMOUNT_MIN = 0;
export const REWARD_DESCRIPTION_MIN = 20;
export const REWARD_RULES = [
    {
        label: 'ALL',
        value: 'ALL',
    },{
        label: 'FIRST',
        value: 'FIRST',
    },{
        label: '20%',
        value: '20%',
    },{
        label: '40%',
        value: '40%',
    },
]

export const MAX_FILE_BYTES_UPLOAD = 4194304;
export const MIB_BYTES = 1048576;

export const GENERAL = {
    precision: 2,
};