import { ENDPOINTS } from "./constants";
import { getRoutePath } from "./utils";
import { messages } from "./i18n";

// to refacto to functional POO
const paramExp = /\{([^}]+)\}/g
    
const checkUrlParams = (requirements, params) => {

    for (let i = 0; i < requirements.length; i++) {
        const element = requirements[i];
        if (!params.hasOwnProperty(element)) {
            console.log(`missed : ${element}`)
            return false
        }
    }

    return true
}

const parseParams = (url, params) => {
    const rawfullContents = url.match(paramExp)
    const rawParams = rawfullContents.map(e => e.slice(1, -1))
    rawParams.forEach((p, index)=> {
        const paramValue =  params[p]
        const fullContent = rawfullContents[index];
        url = url
            .replace(fullContent, paramValue)
    });

    return url
}

const generateEndpoint = (name, params = null) => {
    const endPoint = ENDPOINTS.find(e => e.name === name )
    if (!endPoint) {
        return null
    }

    const requiredParams = endPoint.path
        .match(paramExp)
        ?.map(e => e.slice(1, -1))
    
    if (!requiredParams) {
        return endPoint.path
    }
    if (requiredParams.length && !params) {
        console.log(`${requiredParams.length}  expected, 0 passed`)
        return null
    }

    if (!checkUrlParams(requiredParams, params)) {
        return null
    }

    const url = parseParams(endPoint.path, params)
    
    return url; 
};

//move to utils/routing
const getFirstPath = (fullPath) =>  {
    const trimmedPath = fullPath.replace(/^\/|\/$/g, ''); // Supprime les slashs au début et à la fin
  
    if (trimmedPath.includes('/')) {
        const firstPath = trimmedPath.split('/')[0];
        return firstPath;
    } else {
        return null;
    }
}

export const getUrlLanguage = (fullPath) => {
    const firstPath = getFirstPath(fullPath);
    return Object.keys(messages)
        .find( lang => lang === firstPath);
}
  
window.Routing = {
    generate: generateEndpoint,
    getRoutePath: getRoutePath,
}

export default {
    install: (app, options) => {
        app.config.globalProperties.$routing = window.Routing;
    }
};