import axios from 'axios';
import axiosRetry from 'axios-retry';
import storage from './storage';
import { STATUS } from './constants';

axios.defaults.withCredentials = true;
axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;

axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";

axios.interceptors.response.use(
    (response) => response,
    async (error) => {
        if(error.response && error.response.status == STATUS.HTTP_BAD_REQUEST) {

            const errors = Object.values(error.response.data).reduce(
                (acc, curr) => 
                    [    ...acc,
                        ...curr
                    ]
                , [])
            error.response.errors = errors;
        }
        
        const originalConfig = error.config;

        if (originalConfig.url !== window.Routing.generate('login') && error.response) {
            // Access Token was expired
            
            if (error.response.status === 401 && !originalConfig._retry) {
                
                originalConfig._retry = true;

                try {
                    await storage.dispatch('auth/refresh');
                    
                    return axios(originalConfig);
                } catch (_error) {
                    throw _error;
                }
            }
        }
        throw error
    }
)

const client = axios.create();

axiosRetry(client, {
    retries: 2, // the first request + this 2 = total of 3 requests
    retryDelay: axiosRetry.exponentialDelay,
});

window.$axios = {
    retry: client,
    single: axios,
}

export default {
    install: (app, options) => {
        app.config.globalProperties.$axios = {
            retry: client,
            single: axios,
        }
    }
};
