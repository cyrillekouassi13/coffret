export default {
    props: {
        value: {
            type: String,
        },
        label: {
            type: String,
            default: '',
        },
        name: {
            type: String,
            default: null,
        },
        hint: {
            type: String,
            default: null,
        },
        invalid: {
            type: Boolean,
            default: false,
        },
        disabled: {
            type: Boolean,
            default: false,
        },
        loading: {
            type: Boolean,
            default: false,
        },
        labelPointerEvents: Boolean,
    },
    computed: {
        inputName() {
            return this.name || this.label.toLowerCase().replace(/ /g, '_');
        },
        hasErrors() {
            if (!this.$slots['errors']) {
                return false
            }
            const errors = this.$slots['errors']()
            return errors && errors?.some(vnode => vnode.children[0].type !== undefined);
        },
        formControlFieldClass() {
            return {
                'border-2 border-rose-600': this.hasErrors || this.invalid,
                'disabled': this.disabled,
                'has-postfix-icon': this.loading,
            };
        },
    },
    methods: {
        onInput(event) {
            this.$emit('input', event.target.value);
        },
        onChange(event) {
            this.$emit('change', event.target.value);
        },
        onClick(event) {
            this.$emit('click', event);
        },
    },
};
