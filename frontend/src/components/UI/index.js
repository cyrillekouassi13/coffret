import MButton from './Button.vue';
import MInput from './Input.vue';
import Spinner from './Spinner.vue'

export {
    MButton,
    MInput,
    Spinner,
}