import { getLocale } from "../../utils";

export default {
    namespaced: true,
    state: {
        currentLocale: getLocale()
    },
    mutations: {
        setCurrentLocale: (state, language) => {
            state.currentLocale = language;
        },
    },
}