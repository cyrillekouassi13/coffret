// move to utils !!
function getCookie(cookieName) {
    const cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i].trim();
        if (cookie.startsWith(cookieName + '=')) {
            return cookie.substring(cookieName.length + 1);
        }
    }
    return null;
}
  
export default {
    namespaced: true,
    state: {
        accessToken: getCookie('access_token'),
        refreshToken: getCookie('refresh_token'), 
        loggingIn: false,
        loginError: null,
        isAuth: false
    },
    mutations: {
        loginStart: state => state.loggingIn = true,
        loginStop: (state, errorMessage) => {
            state.loggingIn = false;
            state.loginError = errorMessage;
        },
        updateTokens: (state, tokens) => {
            state.accessToken = tokens?.access;
            state.refreshToken = tokens?.refresh;
        },
        logout(state) {
            state.isAuth = false

        },
        login: (state)=> state.isAuth = true
    },
    actions: {
        authenticate({commit, dispatch}, tokens) {
            commit('updateTokens', tokens)
            if (tokens) {
                commit('login')   
            } else {
                commit('logout')
                dispatch('setUser', undefined, {root: true})
            }
        },
        async refresh({dispatch, state}) {
            if (!state.refreshToken) {
                return
            }

            await $axios.single.post(window.Routing.generate('token_refresh'), {refresh: state.refreshToken})
                .then(response => {
                    dispatch('authenticate', response.data)
                })
                .catch(() => {
                    dispatch('authenticate', undefined)
                })
        }
    },
}