export default {
    namespaced: true,
    state: {
        id: null,
        username: null,
        email: null,
    },
    getters: {
        getId(state) {
            return state.id;
        },
        getUsername(state) {
            return state.username;
        },
    },
    mutations: {
        setId(state, payload) {
            state.id = payload;
        },
        setEmail(state, payload) {
            state.email= payload;
        },
        setUsername(state, payload) {
            state.username = payload;
        },
    },
    actions: {
        async initUser({commit, dispatch, rootState}, reset = false) {
            if(reset) {

            }
            
            if(!rootState.auth.isAuth) {
                return
            }

            try {
                const result = await $axios.single.get(
                    window.Routing.generate('user')
                )
                dispatch('setUser', result.data)
            } catch (error) {
                
            }
        },
        setUser({commit}, user) {
            commit('setUsername', user?.username)
            commit('setEmail', user?.email)
            commit('setId', user?.id)
        }
    }
};
