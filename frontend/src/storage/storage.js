import Mutations from './mutations';
import Actions from './actions';
import user from './modules/user'
import auth from './modules/auth'
import interval from '../utils/interval';
import locale from './modules/locale';

export default {
    state: {
        interval,
    },
    mutations: Mutations,
    actions: Actions,
    modules: {
        user,
        auth,
        locale
    },
};