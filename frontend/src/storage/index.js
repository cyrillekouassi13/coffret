
import { createStore } from 'vuex';
import storage from './storage';

export default createStore(storage);