import { HOME } from "../constants";
import router from "../routers";

export default {
    async doLogin({ commit, dispatch }, {redirectTo = HOME, ...data}) {
        commit('auth/loginStart');
        const result = await $axios.single.post(window.Routing.generate('login'), {
            ...data
        })
        
        const tokens = result.data
        
        commit('auth/loginStop', null);
        dispatch('auth/authenticate', tokens);
        router.push(redirectTo);
    },
    async doRegister({dispatch}, {redirectTo = HOME, ...data}) {
        const result = await $axios.single.post(window.Routing.generate('register'), {
            ...data
        })
        const tokens = result.data;
        dispatch('auth/authenticate', tokens);
            
        router.push(redirectTo);
    },
    logout() {
        $axios.single.post(window.Routing.generate('logout')
        ).then(() => {
            window.location.reload();
        })
    }
};