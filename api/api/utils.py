
from rest_framework.exceptions import ValidationError

def validate_at_least_one_reward(value):
    if not any(value.values()):
        raise ValidationError("vous devez rempli au moins l'un des champs : {}.".format(', '.join(str(value) for value in value.values())))
