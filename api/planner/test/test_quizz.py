import pytest
from django.test import TestCase
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from django.urls import reverse
from planner.models import quizz, questionanswer
from planner.serializers import QuestionAnswerSerializer

@pytest.mark.django_db
class TestQuestionAnswer(TestCase):

    def test_question_str(self):
        question = questionanswer(text_question='What is the capital of France?')
        self.assertEqual(str(question), 'What is the capital of France?')


@pytest.mark.django_db
class TestQuestionAnswerListApiView(APITestCase):

    def setUp(self):
        self.user = User.objects.create(username='testuser', email='cyr13@gmail.com', password='azerty1234$')
        self.client.force_authenticate(user=self.user)
        self.quizz = quizz.objects.create(name='Test Quizz', required_score_to_pass=50, created_by=self.user)

    def test_get_questionanswers_list(self):
        questionanswer.objects.create(quizz=self.quizz, type_answer='Type A', text_question='Question A', correct_answer='Correct A', incorrect_answer=['Incorrect A', 'Incorrect B'])
        questionanswer.objects.create(quizz=self.quizz, type_answer='Type B', text_question='Question B', correct_answer='Correct B', incorrect_answer=['Incorrect C', 'Incorrect D'])

        url = reverse('questionanswers-list', kwargs={'quizz_id': self.quizz.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)

    # questionanswer must be a list  as data
    def test_create_questionanswer(self):
        url = reverse('questionanswers-list', kwargs={'quizz_id': self.quizz.id})
        data = [{
            'type_answer': 'Type C',
            'text_question': 'Question C',
            'correct_answer': 'Correct C',
            'incorrect_answer': ['Incorrect E', 'Incorrect F']
        }]

        response = self.client.post(url, data, format='json')

        if response.status_code != 201:
            print(response.data)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(questionanswer.objects.count(), 1)
        self.assertEqual(questionanswer.objects.first().text_question, 'Question C')


@pytest.mark.django_db
class TestQuestionAnswerDetailApiView(APITestCase):

    def setUp(self):
        self.user = User.objects.create(username='testuser', email='cyr13@gmail.com', password='azerty1234$')
        self.client.force_authenticate(user=self.user)
        self.quizz = quizz.objects.create(name='Test Quizz', required_score_to_pass=50, created_by=self.user)
        self.questionanswer = questionanswer.objects.create(quizz=self.quizz, type_answer='Type A', text_question='Question A', correct_answer='Correct A', incorrect_answer=['Incorrect A', 'Incorrect B'])

    def test_get_questionanswer(self):
        url = reverse('questionanswer-detail', kwargs={'quizz_id': self.quizz.id, 'questionanswer_id': self.questionanswer.id})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text_question'], 'Question A')

    def test_update_questionanswer(self):
        url = reverse('questionanswer-detail', kwargs={'quizz_id': self.quizz.id, 'questionanswer_id': self.questionanswer.id})
        data = {
            'type_answer': 'Type B',
            'text_question': 'Updated Question',
            'correct_answer': 'Updated Correct Answer',
            'incorrect_answer': ['Updated Incorrect A', 'Updated Incorrect B']
        }

        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['text_question'], 'Updated Question')
        self.questionanswer.refresh_from_db()
        self.assertEqual(self.questionanswer.text_question, 'Updated Question')

    def test_delete_questionanswer(self):
        url = reverse('questionanswer-detail', kwargs={'quizz_id': self.quizz.id, 'questionanswer_id': self.questionanswer.id})

        response = self.client.delete(url)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(questionanswer.objects.filter(id=self.questionanswer.id).exists())

