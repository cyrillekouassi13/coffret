import pytest
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import date, time, timedelta
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from planner.models import challenge
from planner.serializers import ChallengeSerializer

from datetime import time
import datetime


@pytest.fixture
def create_challenge():
    def _create_challenge(name, description, time_challenge, start, end, complexity, created_by):
        return challenge.objects.create(name=name, description=description, time_challenge=time_challenge, start=start,
                                        end=end, complexity=complexity, created_by=created_by)
    return _create_challenge


@pytest.mark.django_db
def test_is_active(create_challenge):
    active_challenge = create_challenge(name='Active Challenge', description='Active Description',
                                        time_challenge=time(12, 0), start=date.today(),
                                        end=date.today() + timedelta(days=7),
                                        complexity='Easy', created_by=User.objects.create(username='planner1', email='cyr@gmail.com', password='azer1234*'))

    inactive_challenge = create_challenge(name='Inactive Challenge', description='Inactive Description',
                                          time_challenge=time(12, 0), start=date.today() - timedelta(days=7),
                                          end=date.today() - timedelta(days=1),
                                          complexity='Medium', created_by=User.objects.create(username='planner2', email='cyr@gmail.com', password='azer1234*'))

    assert active_challenge.is_active()
    assert not inactive_challenge.is_active()


@pytest.mark.django_db
def test_is_time_valid(create_challenge):
    valid_challenge = create_challenge(name='Valid Challenge', description='Valid Description',
                                       time_challenge=time(12, 0), start=timezone.now().date(),
                                       end=date.today() + timezone.timedelta(hours=7),
                                       complexity='Hard', created_by=User.objects.create(username='planner3', email='cyr@gmail.com', password='azer1234*'))

    invalid_challenge = create_challenge(name='Invalid Challenge', description='Invalid Description',
                                         time_challenge=time(12, 0),
                                         start=timezone.now().date(), end=date.today() + timezone.timedelta(days=7),
                                         complexity='Suprise', created_by=User.objects.create(username='planner4', email='cyr@gmail.com', password='azer1234*'))

    assert valid_challenge.is_time_valid()
    assert not invalid_challenge.is_time_valid()



class ChallengeListApiViewTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='planner3', email='cyr@gmail.com', password='azer1234*')
        self.client.force_authenticate(user=self.user)
        
        self.url = "http://localhost:8082/api/planner/challenge/"  # Assuming 'challenge-list' is the URL name for the API view

    def test_get_challenges(self):
        # Create some challenges for the authenticated user
        challenge1 = challenge.objects.create(name='Challenge 1', description='Challenge 1 description',
                                              image='null', time_challenge=datetime.time(1,0,0,0),
                                              start='2023-06-01', end='2023-06-10',
                                              complexity='Medium', created_by=self.user)

        challenge2 = challenge.objects.create(name='Challenge 2', description='Challenge 2 description',
                                              image='null', time_challenge=datetime.time(1,0,0,0),
                                              start='2023-06-05', end='2023-06-15',
                                              complexity='Easy', created_by=self.user)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Ensure the challenges returned match the ones created for the user
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['name'], challenge1.name)
        self.assertEqual(response.data[1]['name'], challenge2.name)

    def test_create_challenge(self):

        
        data = {
            'name': 'New Challenge',
            'description': 'New Challenge description',
            'image': 'null',
            'time_challenge': datetime.time(1,0,0,0),
            'start': '2023-06-20',
            'end': '2023-06-30',
            'complexity': 'Hard',
            'created_by':self.user
        }

        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Ensure the challenge is created with the correct data
        created_challenge = challenge.objects.get(id=response.data['id'])
        self.assertEqual(created_challenge.name, data['name'])
        self.assertEqual(created_challenge.description, data['description'])
        self.assertEqual(created_challenge.image, data['image'])
        self.assertEqual(created_challenge.time_challenge, data['time_challenge'])
        self.assertEqual(created_challenge.start.strftime('%Y-%m-%d'), data['start'])
        self.assertEqual(created_challenge.end.strftime('%Y-%m-%d'), data['end'])
        self.assertEqual(created_challenge.complexity, data['complexity'])
        self.assertEqual(created_challenge.created_by, self.user.id)
