from django.shortcuts import render
from django.http import HttpResponse
import json
from rest_framework.permissions import AllowAny
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from .models import challenge, reward, quizz,  coffret, invitation, score, questionanswer
from .serializers import ChallengeSerializer, RewardSerializer, QuizzSerializer, CoffretSerializer, InvitationSerializer, ScoreSerializer, UserSerializer, LoginSerializer, QuestionAnswerSerializer
from django.shortcuts import get_object_or_404

 # Todo : https://github.com/ryazantsevmaxim-main/JwtAuthNextDjango
class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening

# Manage Challenge
class ChallengeListApiView(APIView):
    # Todo : add permission to check if planner is authenticated
    serializer_class = ChallengeSerializer
    permission_classes = [permissions.IsAuthenticated]

    # 1. List all
    def get(self, request):
        '''
        List all the challenge for given requested user
        '''
        challenges = challenge.objects.filter(created_by = request.user.id)
        serializer = ChallengeSerializer(challenges, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 2. Create or post
    def post(self, request, *args, **kwargs):
        '''
        Create the challenge with given challenge data
        '''
        data = {
            'title': request.data.get('title'),  
            'description':request.data.get('description'), 
            'image':request.data.get('image'),  
            'time':request.data.get('time'),  
            'start':request.data.get('start'),  
            'end': request.data.get('end'), 
            'complexity': request.data.get('complexity'), 
            'created_by' : request.user.id, 
        }
        serializer = ChallengeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ChallengeDetailApiView(APIView):
    # add permission to check if user is authenticated
    serializer_class = ChallengeSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, challenge_id, created_by_id):
        '''
        Helper method to get the object with given challenge_id, and people who creates challenges
        '''
        try:
            return challenge.objects.get(id=challenge_id, created_by = created_by_id)
        except challenge.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, challenge_id, *args, **kwargs):
        '''
        Retrieves the challenge with given challenge_id
        '''
        challenge_instance = self.get_object(challenge_id, request.user.id)
        if not challenge_instance:
            return Response(
                {"response":"this challenge does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ChallengeSerializer(challenge_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    # 4. Update
    def put(self, request, challenge_id, *args, **kwargs):
        '''
        Updates the challenge with given challenge_id if exists
        '''
        challenge_instance = self.get_object(challenge_id, request.user.id)
        if not challenge_instance:
            return Response(
                {"response": "Object with challenge id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'title': request.data.get('title'),  
            'description':request.data.get('description'), 
            'image':request.data.get('image'),  #Todo : image charge doesnt work
            'time':request.data.get('time'),  
            'start':request.data.get('start'),  
            'end': self.request.data.get('end'), 
            'complexity': request.data.get('complexity'), 
            'created_by' : request.user.id, 
        }
        serializer = ChallengeSerializer(instance = challenge_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  

    # 5. Delete
    def delete(self, request, challenge_id, *args, **kwargs):
        '''
        Deletes the challenge id with given challenge_id if exists
        '''
        challenge_instance = self.get_object(challenge_id, request.user.id)
        if not challenge_instance:
            return Response(
                {"response": "Object with challenge id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        challenge_instance.delete()
        return Response(
            {"res": "Object deleted! "},
            status=status.HTTP_200_OK
        )
    

# Manage reward

class RewardListApiView(APIView):
    # add permission to check if user is authenticated
    serializer_class = RewardSerializer
    permission_classes = [permissions.IsAuthenticated]


     # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the reward for given requested user
        '''
        rewards = reward.objects.filter(created_by = request.user.id)
        serializer = RewardSerializer(rewards, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 2. Create or post
    def post(self, request, *args, **kwargs):
        '''
        Create the reward with given reward data
        '''
    
        data = {
            'name': request.data.get('name'),   
            'description':request.data.get('description'),  
            'type_image':request.data.get('type_image'),  
            'type_amount':request.data.get('type_amount'),  # Todo : see the logic of amount
            'rule': request.data.get('rule'), 
            'created_by': request.user.id, 
        }
        serializer = RewardSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(created_by=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class RewardDetailApiView(APIView):
    # add permission to check if user is authenticated
    serializer_class = RewardSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, reward_id, created_by_id):
        '''
        Helper method to get the object with given reward_id, and user_id
        '''
        try:
            return reward.objects.get(id=reward_id, created_by = created_by_id)
        except reward.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, reward_id, *args, **kwargs):
        '''
        Retrieves the reward with given reward_id
        '''
        reward_instance = self.get_object(reward_id, request.user.id)
        if not reward_instance:
            return Response(
                {"response":"this reward does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = RewardSerializer(reward_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    # 4. Update
    def put(self, request, reward_id, *args, **kwargs):
        '''
        Updates the reward with given reward_id if exists
        '''
        reward_instance = self.get_object(reward_id, request.user.id)
        if not reward_instance:
            return Response(
                {"response": "Object with reward id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )


        serializer = RewardSerializer(instance = reward_instance, data=request.data, partial = True)
        if serializer.is_valid():
            serializer.save(created_by=request.user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  

    # 5. Delete
    def delete(self, request, reward_id, *args, **kwargs):
        '''
        Deletes the rewards id with given reward_id if exists
        '''
        reward_instance = self.get_object(reward_id, request.user.id)
        if not reward_instance:
            return Response(
                {"response": "Object with reward id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        reward_instance.delete()
        return Response(
            {"response": "deleted!"},
            status=status.HTTP_200_OK
        )
    
# Manage quizz and questions

class QuizzListApiView(APIView):
# add permission to check if user is authenticated
    serializer_class = QuizzSerializer
    permission_classes = [permissions.IsAuthenticated]

     # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the quizz for given requested user
        '''
        quizzs = quizz.objects.filter(created_by = request.user.id)
        serializer = QuizzSerializer(quizzs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 2. Create or post
    def post(self, request, *args, **kwargs):
        '''
        Create the quizz with given quizz data
        '''
        serializer = QuizzSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(created_by=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class QuizzDetailApiView(APIView):
    # add permission to check if user is authenticated
    serializer_class = QuizzSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, quizz_id, created_by_id):
        '''
        Helper method to get the object with given quizz_id, and user_id
        '''
        try:
            return quizz.objects.get(id = quizz_id, created_by = created_by_id)
        except quizz.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, quizz_id):
        '''
        Retrieves the quizz with given quizz_id
        '''
        quizz_instance = self.get_object(quizz_id, request.user.id)
        if not quizz_instance:
            return Response(
                {"response":"this quizz does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = QuizzSerializer(quizz_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    # 4. Update
    def put(self, request, quizz_id, *args, **kwargs):
        '''
        Updates the quizz with given quizz_id if exists
        '''
        quizz_instance = self.get_object(quizz_id, request.user.id)
        if not quizz_instance:
            return Response(
                {"response": "Object with quizz id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = QuizzSerializer(instance = quizz_instance, data=request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  

    # 5. Delete
    def delete(self, request, quizz_id):
        '''
        Deletes the quizz id with given quizz if exists
        '''
        quizz_instance = self.get_object(quizz_id, request.user.id)
        if not quizz_instance:
            return Response(
                {"response": "Object with quizz id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        quizz_instance.delete()
        return Response(
            {"response": "Object deleted!"},
            status=status.HTTP_200_OK
        )
    
class QuestionAnswerListApiView(APIView):
# add permission to check if user is authenticated
    serializer_class = QuestionAnswerSerializer
    permission_classes = [permissions.IsAuthenticated]

      # 1. List all
    def get(self, request,quizz_id):
        '''
        List all the coffret for given requested user
        '''
        q = get_object_or_404(quizz, id=quizz_id, created_by=request.user.id)
        questionanswers = questionanswer.objects.filter(quizz = q.id)
        serializer = QuestionAnswerSerializer(questionanswers, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK) 
    
    def post(self, request, quizz_id):
        '''
        Create a coffret with given reward data
        '''
        instance_quizz = get_object_or_404(quizz, id=quizz_id, created_by = request.user.id)
        serializer = QuestionAnswerSerializer(data=request.data, many=True)

        if serializer.is_valid():
            serializer.save(quizz = instance_quizz)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class QuestionAnswerDetailApiView(APIView):
 # add permission to check if user is authenticated
    serializer_class = QuestionAnswerSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, quizz_id,questionanswer_id, created_by_id):
        '''
        Helper method to get the object with given coffret_id, and user_id
        '''
        try:
            q = get_object_or_404(quizz, id=quizz_id)
            return questionanswer.objects.get(id = questionanswer_id, quizz= q.id)
        except questionanswer.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, quizz_id,questionanswer_id):
        '''
        Retrieves the coffret with given coffret_id
        '''
        coffret_instance = self.get_object(quizz_id,questionanswer_id, request.user.id)
        if not coffret_instance:
            return Response(
                {"response":"this question-answer doesn't exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = QuestionAnswerSerializer(coffret_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, quizz_id, questionanswer_id):
        '''
        Updates the coffret with given coffret_id if exists
        '''
        questionanswer_instance = self.get_object(quizz_id, questionanswer_id, request.user.id)
        if not questionanswer_instance:
            return Response(
                {"response": "Object with question-answer id doesn't exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = QuestionAnswerSerializer(instance = questionanswer_instance, data=request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  

    # 5. Delete
    def delete(self, request, quizz_id, questionanswer_id):
        '''
        Deletes the coffret id with given coffret_id if exists
        '''
        questionanswer_instance = self.get_object( quizz_id, questionanswer_id, request.user.id)
        if not questionanswer_instance:
            return Response(
                {"response": "Object with question-answer id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        questionanswer_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )
 

    

class CoffretListApiView(APIView):

# add permission to check if user is authenticated
    serializer_class = CoffretSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, created_by_id):
        '''
        Helper method to get the object with given coffret_id, and user_id
        '''
        try:
            return coffret.objects.get(created_by = created_by_id)
        except coffret.DoesNotExist:
            return None

      # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the coffret for given requested user
        '''
        coffrets = coffret.objects.filter(created_by = request.user.id)
        serializer = CoffretSerializer(coffrets, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK) 

    # 2. Create or post
    def post(self, request, *args, **kwargs):
        '''
        Create a coffret with given reward data
        '''
        
        data = {
            'challenge': request.data.get('challenge'),   
            'reward': request.data.get('reward'), 
            'quizz': request.data.get('quizz'), 
            'created_by' : request.user.id,
            'etat' : request.data.get('etat')
        }
        serializer = CoffretSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

  
class CoffretDetailApiView(APIView):

 # add permission to check if user is authenticated
    serializer_class = CoffretSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, coffret_id, created_by_id):
        '''
        Helper method to get the object with given coffret_id, and user_id
        '''
        try:
            return coffret.objects.get(id = coffret_id, created_by = created_by_id)
        except coffret.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, coffret_id, *args, **kwargs):
        '''
        Retrieves the coffret with given coffret_id
        '''
        coffret_instance = self.get_object(coffret_id, request.user.id)
        if not coffret_instance:
            return Response(
                {"response":"this coffret does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = CoffretSerializer(coffret_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, coffret_id, *args, **kwargs):
        '''
        Updates the coffret with given coffret_id if exists
        '''
        coffret_instance = self.get_object(coffret_id, request.user.id)
        if not coffret_instance:
            return Response(
                {"response": "Object with coffret id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'challenge': request.data.get('challenge'),  
            'reward':request.data.get('reward'),  
            'quizz': request.data.get('quizz'), 
            'created_by' : request.user.id,
            'etat' : request.data.get('etat')
        }
        serializer = CoffretSerializer(instance = coffret_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  

    # 5. Delete
    def delete(self, request, coffret_id, *args, **kwargs):
        '''
        Deletes the coffret id with given coffret_id if exists
        '''
        coffret_instance = self.get_object(coffret_id, request.user.id)
        if not coffret_instance:
            return Response(
                {"response": "Object with coffret id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        coffret_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )

# Manage Invitation

class InvitationListApiView(APIView):

    # add permission to check if user is authenticated
    serializer_class = InvitationSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, coffret_id, created_by_id):
        '''
        Helper method to get the object with given invitation_id, and user_id
        '''
        coffret_id =  get_object_or_404(coffret, id=coffret_id, created_by= created_by_id)
        try:
            return invitation.objects.get(coffret= coffret_id.id)
        except invitation.DoesNotExist:
            return None

    # 1. List all invitation related to a challenge
    def get(self, request, coffret_id, *args, **kwargs):
        '''
        List all the invitations for given requested user
        '''
        coffret_id =  get_object_or_404(coffret, id=coffret_id, created_by= request.user.id)
        invitations = invitation.objects.filter(coffret = coffret_id.id)
        serializer = InvitationSerializer(invitations, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Todo : envoyer une seule invitation par participant pour un coffret donné
    # Todo : Gestion des coffrets dans la methode post
    # 2. Create or post
    def post(self, request,coffret_id, *args, **kwargs):
        '''
        Create the coffret with given quizz data
        '''
        # verify if coffret id exists
        coffret_id = get_object_or_404(coffret, id=coffret_id)
        participant = request.data.get('participant')
        send_by = request.user.id
        etat = request.data.get('etat')
        
        if int(participant) == int(request.user.id):
            return Response({"response": "You cannot invite yourself."}, status=status.HTTP_400_BAD_REQUEST)

        data = {
            'participant': participant,
            'coffret': coffret_id.id,
            'send_by': send_by,
            'etat': etat,
        }


        if invitation.objects.filter(participant=participant, coffret=coffret_id.id).exists():
            return Response({"reponse":"invitation already exist"})
        else:
            serializer = InvitationSerializer(data=data)
            # on verifie si le participant a deja reçu l'invit si oui on met un message d'erreur
            if serializer.is_valid():
                serializer.save(coffret=coffret_id)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class  InvitationDetailApiView(APIView):

 # add permission to check if user is authenticated
    serializer_class = InvitationSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, coffret_id ,invitation_id ,send_by_id):
        '''
        Helper method to get the object with given invitation_id, and user_id
        '''
        try:
            return invitation.objects.get(id = invitation_id, coffret=coffret_id, send_by = send_by_id)
        except invitation.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, coffret_id, invitation_id, *args, **kwargs):
        '''
        Retrieves the invitation with given coffret_id
        '''
        invitation_instance = self.get_object(coffret_id, invitation_id, request.user.id)
        if not invitation_instance:
            return Response(
                {"response":"this invitation does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = InvitationSerializer(invitation_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, coffret_id, invitation_id, *args, **kwargs):
        '''
            Updates the invitation with given coffret_id if exists
        '''
        invitation_instance = self.get_object(coffret_id, invitation_id, request.user.id)
        if not invitation_instance:
            return Response(
                {"response": "Object with invitation id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'participant': request.data.get('participant'),  
            'coffret': coffret_id,  
            'send_by' : request.user.id,
            'etat' : request.data.get('etat')
        }
        serializer = InvitationSerializer(instance = invitation_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  

    # 5. Delete
    def delete(self, request, coffret_id, invitation_id, *args, **kwargs):
        '''
        Deletes the invitation id with given coffret_id and invitation_id if exists
        '''
        invitation_instance = self.get_object(coffret_id, invitation_id, request.user.id)
        if not invitation_instance:
            return Response(
                {"response": "Object with invitation id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        invitation_instance.delete()
        return Response(
            {"res": "Object deleted ! "},
            status=status.HTTP_200_OK
        )
    
class ScoreListApiView(APIView):

    # Add permission to check if user is authenticated
    serializer_class = ScoreSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, coffret_id ,invitation_id ,send_by_id):
        '''
        Helper method to get the object with given coffret_id, and user_id
        '''
        try:
            return coffret.objects.get(id = coffret_id)
        except coffret.DoesNotExist:
            return None

    # 1. List all invitation related to a challenge
    def get(self, request, coffret_id, *args, **kwargs):
        '''
        List all the score for all participants
        '''
        coffret_id =  get_object_or_404(coffret, id=coffret_id, created_by= request.user.id)
        scores = score.objects.filter(coffret = coffret_id.id)
        serializer = ScoreSerializer(scores, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

#Todo : the score wil be only view by participant and planner in this case it's viewed by planner
class ScoreDetailApiView(APIView):

    # Add permission to check if user is authenticated
    serializer_class = ScoreSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self, request, coffret_id, score_id, *args, **kwargs):
        '''
        Helper method to get the object with given coffret_id, and score_id
        '''

        coffret_id =  get_object_or_404(coffret, id=coffret_id, created_by= request.user.id)
        try:
            return score.objects.get(id = score_id, coffret = coffret_id.id)
        except score.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, coffret_id, score_id, *args, **kwargs):
        '''
        Retrieves score with given coffret_id
        '''
        score_instance = self.get_object(coffret_id, score_id)
        if not score_instance:
            return Response(
                {"response":"this score request does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ScoreSerializer(score_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update --> forbidden and generate by signal
    def put(self, request, *args, **kwargs):
        '''
        Put the score id with given coffret_id and invitation_id if exists
        '''
        return Response(
            {"res": "Will be generated by a signal"},
            status=status.HTTP_200_OK
        )
  

    # 5. Delete
    def delete(self, request, *args, **kwargs):
        '''
        Deletes the invitation id with given coffret_id and invitation_id if exists
        '''
        return Response(
            {"res": "Will be destroyed by guest"},
            status=status.HTTP_200_OK
        )    

