from django.urls import include, path, re_path
from django.views.decorators.csrf import csrf_exempt
# Api Routes
from .views import *

urlpatterns = [
    # route api
    path('challenge/', ChallengeListApiView.as_view()),
    path('challenge/<int:challenge_id>/', ChallengeDetailApiView.as_view()),

    path('reward/', RewardListApiView.as_view()),
    path('reward/<int:reward_id>/', RewardDetailApiView.as_view()),

    path('quizz/', QuizzListApiView.as_view()),
    path('quizz/<int:quizz_id>/', QuizzDetailApiView.as_view()),

    path('quizz/<int:quizz_id>/questions-answers/', QuestionAnswerListApiView.as_view(), name='questionanswers-list'),

    path('quizz/<int:quizz_id>/question-answer/<int:questionanswer_id>/', QuestionAnswerDetailApiView.as_view(), name='questionanswer-detail'),


    path('coffret/', CoffretListApiView.as_view()),
    path('coffret/<int:coffret_id>/', CoffretDetailApiView.as_view()),

    path('coffret/<int:coffret_id>/invitation/', InvitationListApiView.as_view()),
    path('coffret/<int:coffret_id>/invitation/<int:invitation_id>/', InvitationDetailApiView.as_view()),


    # Todo : 
    # Faire le crud sur la partie score
    # Supprimer after :
    # create , update, delete
    path('score/<int:coffret_id>/participant/ranking/', ScoreListApiView.as_view())


    #score detail participant apres un challenge
    # score/<int:coffret_id>/participant/ranking
    # score/<int:coffret_id>/participant/<int:participant_id>
    # coffret/<int:coffret_id>/participant/<int:participant>/
    
]