from .models import challenge, reward, quizz, coffret, invitation, score, questionanswer
from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from django.core.files import File
User=get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """
        User serializer is created for User 
    """
    class Meta:
        model = User
        fields = ['id','username', 'password', 'email']


class LoginSerializer(serializers.Serializer):
    """
        serializer for User Login 
    """

    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(username=attrs['username'], password=attrs['password'])

        if not user:
            raise serializers.ValidationError('Incorrect email or password.')

        if not user.is_active:
            raise serializers.ValidationError('User is disabled.')

        return {'user': user}

class OptionalImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str):
            # Return the input as is if it is a string
            return data
        return super().to_internal_value(data)


class ChallengeSerializer(serializers.ModelSerializer):
    """
        serializer for Challenge 
    """
    LEVEL_CHOICES = (
        ('Suprise', 'Suprise'),
        ('Easy', 'Easy'),
        ('Medium', 'Medium'),
        ('Hard', 'Hard'),
    )

    complexity = serializers.ChoiceField(
                        choices = LEVEL_CHOICES)
    image = OptionalImageField(required=False)
    class Meta:
        model = challenge
        fields = ["id", "name", "description", "image", "time_challenge", "start", "end", "complexity", "created_by"]

class OptionalImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str):
            # Return the input as is if it is a string
            return data
        return super().to_internal_value(data)


class RewardSerializer(serializers.ModelSerializer):
    """
        serializer for Reward
    """
    REWARD_RULES = [
        ('Image', (
            ('ALL', 'All'),
            ('FIRST', 'First'),
        )
         ),
        ('Amount', (
            ('ALL', 'All'),
            ('FIRST', 'First'),
            ('20%','20%'),
            ('40%','40%')
        )
         )
    ]
    rule = serializers.ChoiceField(
                        choices = REWARD_RULES) 
    
    # tips : here https://www.youtube.com/watch?v=ZAWetS-LB8c
    # https://stackoverflow.com/questions/35522768/django-serializer-imagefield-to-get-full-url
    type_image = OptionalImageField(required=False)
    type_amount = serializers.IntegerField(default=0, initial=0)
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = reward
        fields = ["id", "name","description", "type_image", "type_amount", "type_text", "rule", "created_by"]

class QuizzSerializer(serializers.ModelSerializer):
    """
        serializer for Quizz
    """
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = quizz
        fields = ["id", "name", "required_score_to_pass", "created_by"]

class QuestionAnswerSerializer(serializers.ModelSerializer):
    '''
        serializer for answer question
    '''
    quizz = QuizzSerializer(read_only=True)
    class Meta:
        model = questionanswer
        fields = "__all__"



class CoffretSerializer(serializers.ModelSerializer):
    """
        serializer for Coffret
    """    
    #Todo : if challenge date is finished coffret etat becomes True 

    created_by = UserSerializer(read_only=True)
    class Meta:
        model = coffret 
        fields = ["id", "challenge", "reward", "quizz", "created_by", "etat"]


class InvitationSerializer(serializers.ModelSerializer):
    """
        serializer for Invitation
    """
    send_by =  UserSerializer(read_only=True)
    class Meta:
        model = invitation
        fields = ["id", "participant", "coffret", "send_by", "etat"]
                  

class ScoreSerializer(serializers.ModelSerializer):
    """
        serializer for Score
    """

    coffret = CoffretSerializer(read_only=True)
    participant =  UserSerializer()
    class Meta:
        model = score
        fields = ["id", "quizz_score", "coffret", "participant"]
