from django.contrib import admin
from planner.models import *
# Register your models here.

admin.site.register(payment)
admin.site.register(planner)
admin.site.register(challenge)
admin.site.register(reward)
admin.site.register(participant)
admin.site.register(questionanswer)

class QuizzInline(admin.TabularInline):
    model = quizz

admin.site.register(quizz)

admin.site.register(coffret)
admin.site.register(score)
admin.site.register(invitation)

