from django.contrib.auth import get_user_model
from django.core.validators import MinLengthValidator
from django.db import models
from django.contrib.auth import get_user_model
from api.utils import validate_at_least_one_reward
from django.utils import timezone
from datetime import date, time, timedelta


User=get_user_model()
# Create your models here.

# Todo : use package payment
class payment(models.Model):
    """
        A payment class to allow send or receive cash
    """
    cc_number = models.CharField(max_length=100)
    cc_expiry = models.CharField(max_length=100)
    cc_code = models.CharField(max_length=100)

    def __str__(self):
        return "%s-%s" % (self.cc_number, self.cc_expiry)
class planner(models.Model):
    """
        Planner is a person who create the coffret and organize challenges
    """

    planner = models.ForeignKey(User, on_delete=models.CASCADE)
    creditcard = models.ForeignKey(payment, on_delete=models.CASCADE)
    biography= models.TextField()
    def __str__(self):
        return f'{self.planner}'
    
class challenge(models.Model):
    """
        Challenge is created by a planner and send to the partiicpants
    """

    LEVEL_CHOICES = (
        ('Suprise', 'Suprise'),
        ('Easy', 'Easy'),
        ('Medium', 'Medium'),
        ('Hard', 'Hard'),

    )

    name = models.CharField(max_length=250)
    description = models.TextField()
    image = models.ImageField(upload_to ='challenges/', default='/coffre.png')
    time_challenge = models.TimeField()
    start = models.DateField()
    end = models.DateField()
    complexity = models.CharField(max_length=20, choices=LEVEL_CHOICES, default='Suprise')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
 
    # date validation rule end>start
    # add defaut image to my challenge
    # create an folder to store image of challenge of each planner
    def __str__(self):
        return self.name
    
    def is_active(self):
        """
        Checks if the challenge is currently active.
        """
        current_date = timezone.now().date()
        return self.start <= current_date <= self.end
    
    def is_time_valid(self):
        """
        Checks if the current time is within the start and end time of the challenge.
        """
        time_challenge = self.time_challenge
        res =  int(self.end) - int(self.start)

        return res 


class reward(models.Model):
    """
        The thing that you win when you succeed to a quizz
    """

    REWART_TEXT_MIN_LENGTH = 20
    
    REWARD_RULES = [
        ('Winner', (
            ('ALL', 'All'),
            ('FIRST', 'First'),
            ('20%','20%'),
            ('40%','40%')
        )
         )
    ]

    name = models.CharField(max_length=250)
    description = models.TextField()
    type_image = models.ImageField(upload_to ='rewards/', null=True, blank=True)
    type_amount = models.IntegerField(blank=True,default=0)
    type_text = models.CharField(max_length=250, blank=True)
    rule =  models.CharField(max_length=150, choices=REWARD_RULES)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def clean(self):
        validate_at_least_one_reward({
            'reward_amount': self.reward_amount,
            'reward_image': self.reward_image,
            'reward_text': self.reward_text,
        })

    # create an folder to store image of reward of each planner
    # if amount > 0 it will be send to the winner or baded on the ranking after the challenge
    
class quizz(models.Model):
    """
        Quizz is created by a planner and solve by the participants
    """
    name = models.CharField(max_length=250)
    required_score_to_pass = models.IntegerField(help_text="required score in %")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'
        
class questionanswer(models.Model):
    quizz = models.ForeignKey(quizz, on_delete=models.CASCADE)
    type_answer = models.CharField(max_length=250)
    text_question = models.CharField(max_length=250)
    correct_answer = models.CharField(max_length=200)
    incorrect_answer = models.JSONField()

    def __str__(self):
        return f'{self.text_question}'


class  participant(models.Model):
    """
        Participant is a people who participates to challenges
    """

    participant = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.participant}'

class coffret(models.Model):
    """
        Coffret is locked and contain quizz and reward and challenge
    """

    challenge = models.ForeignKey(challenge, on_delete=models.CASCADE)
    reward = models.ForeignKey(reward, on_delete=models.CASCADE)
    quizz = models.ForeignKey(quizz, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    open = models.BooleanField(default=False)
    
    def __str__(self):
        return f'{self.challenge.title}'

class score(models.Model):
    """
        Score is obtained by participant after solving a quizz
    """

    quizz_score = models.IntegerField(default=0)
    coffret = models.ForeignKey(coffret, on_delete=models.CASCADE)
    participant = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.participant} - {self.quizz_score}'


class invitation(models.Model):
    """
        Invitation is send by planner to participants
    """

    INVITATION_STATUS = [
        ('In_progress', 'In_progress'),
        ('Accepted', 'Accepted'),
        ('Refused', 'Refused')
    ]    

    participant = models.ForeignKey(User, on_delete=models.CASCADE, related_name='participants')
    coffret = models.ForeignKey(coffret, on_delete=models.CASCADE)
    send_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    etat = models.CharField(max_length=20, choices=INVITATION_STATUS, default='In_progress')

    def __str__(self):
        return f'{self.participant.username}'

