from .models import responsequizz
from rest_framework import serializers
from django.contrib.auth import get_user_model
User=get_user_model()


class UpdateListSerializer(serializers.ListSerializer):
  
    def update(self, instances, validated_data):
      
        instance_hash = {index: instance for index, instance in enumerate(instances)}

        result = [
            self.child.update(instance_hash[index], attrs)
            for index, attrs in enumerate(validated_data)
        ]

        return result

class ResponseQuizzSerializer(serializers.ModelSerializer):

    def update(self, instance, validated_data):
        instance.response =  validated_data["response"]
        return instance
    
    class Meta:
        model = responsequizz
        fields = ["id", "edited_by", "question", "response"]
        

