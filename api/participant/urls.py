from django.urls import include, path, re_path
from django.views.decorators.csrf import csrf_exempt
from .views import * 
from .scoring import *


urlpatterns = [
    path('invitation/', InvitationListApiView.as_view()),
    path('invitation/<int:invitation_id>/', InvitationDetailApiView.as_view()),
    path('invitation/<int:invitation_id>/coffret/', DetailChallengeCoffret.as_view()),
    path('invitation/<int:coffret_id>/solving/', SolvingQuizz.as_view()),
    path('invitation/<int:coffret_id>/sol/', MyModelListUpdateAPIView.as_view()),
    path('invitation/<int:coffret_id>/sole/', DemoViewset.as_view()),
     #path('<int:coffret_id>/scoring/', Scoring.as_view()),

    ]

# Détail coffret invitation (1) OK

# Détail challenge (2) OK

# Résoudre Challenge --> lancer le quizz (3) En cours update mutiple response quizz OK

# Verifier son score après un challenge (4)