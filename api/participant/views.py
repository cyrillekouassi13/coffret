from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404, render

from planner.models import invitation, challenge, coffret, questionanswer, quizz
from .models import responsequizz
from planner.serializers import InvitationSerializer, ChallengeSerializer, CoffretSerializer
from participant.serializers import ResponseQuizzSerializer

# importer les challenges dans planner

# Create your views here.

# récuprération de toutes les invitations
    
class InvitationListApiView(APIView):
    # add permission to check if planner is authenticated
    serializer_class = InvitationSerializer
    permission_classes = [permissions.IsAuthenticated or permissions.AllowAny]

    # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the invitation send by a planner to a participant
        '''
        participants = invitation.objects.filter(participant = request.user.id)
        serializer = InvitationSerializer(participants, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class  InvitationDetailApiView(APIView):

 # add permission to check if user is authenticated
    serializer_class = InvitationSerializer
    permission_classes = [permissions.IsAuthenticated]
   
    def get_object(self,invitation_id ,participant_id):
        '''
        Helper method to get the object with given invitation_id, and user_id
        '''
        try:
            return invitation.objects.get(id = invitation_id, participant=participant_id)
        except invitation.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, invitation_id, *args, **kwargs):
        '''
        Retrieves the invitation with given challenge_id
        '''
        invitation_instance = self.get_object(invitation_id, request.user.id)
        if not invitation_instance:
            return Response(
                {"response":"this invitation does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = InvitationSerializer(invitation_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Todo : Allow Update only Status
    def put(self, request, invitation_id, *args, **kwargs):
        '''
        Updates the challenge with given challenge_id if exists
        '''
        invitation_instance = self.get_object(invitation_id, request.user.id)
        if not invitation_instance:
            return Response(
                {"response": "Object with invitation id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
            'send_by' : request.user.id,
            'status' : request.data.get('status')
        }
        serializer = InvitationSerializer(instance = invitation_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
class DetailChallengeCoffret(APIView):
    # add permission to check if planner is authenticated
    serializer_class = InvitationSerializer
    permission_classes = [permissions.IsAuthenticated or permissions.AllowAny]

    def get_object(self,challenge_id ,participant_id):
        '''
        Helper method to get the object with given invitation_id, and user_id
        '''
        try:
            return challenge.objects.get(id = challenge_id)
        except invitation.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, invitation_id, *args, **kwargs):
        '''
        Retrieves the invitation with given challenge_id
        '''
        invitations=  get_object_or_404(invitation, id=self.kwargs['invitation_id'])
        challenge_id = invitations.coffret.challenge.id
        challenge_instance = self.get_object(challenge_id, request.user.id)
        if not challenge_instance or (invitations.status != "In_progress"):
            return Response(
                {"response":"this challenge doesn't exist or verify the status of the invitation"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ChallengeSerializer(challenge_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)


class SolvingQuizz(APIView):
    """
    solving quizz
    """

    # add permission to check if planner is authenticated
    serializer_class = ResponseQuizzSerializer
    permission_classes = [permissions.IsAuthenticated or permissions.AllowAny]

    def get_object(self,participant_id, coffret_id):
        '''
        Helper method to get the object with given invitation_id, and user_id
       
        coffret_instance = get_object_or_404(coffret, id=coffret_id)
        print(coffret_instance.quizz)
        question_instance =  get_object_or_404(question, quizz= coffret_instance.quizz)
        print(question_instance.id)
        try:
            return responsequizz.objects.filter(edited_by = participant_id, question =  question_instance)
        except responsequizz.DoesNotExist:
            return None
         '''
    # Todo : filtre sur plusieurs instances 
    def get(self, request, coffret_id, *args, **kwargs):
        # on recupère les questions d'un quizz qui appartient à un coffret
        coffret_instance = get_object_or_404(coffret, id=coffret_id)
        question_instance = question.objects.filter(quizz=coffret_instance.quizz.id)
        queryset = responsequizz.objects.all().filter(question__in=question_instance)
        serializer = ResponseQuizzSerializer(queryset,many=True)
        return Response(serializer.data)


    # spécifique au coffret et à l'invitation si l'invit est acceptée on checke le coffret
    # https://stackoverflow.com/questions/60258388/how-to-update-multiple-objects-in-django-rest-framework
    def put(self, request, coffret_id, *args, **kwargs):
        invitation_instance = get_object_or_404(invitation, participant=request.user, coffret=coffret_id)
        quizz_id = get_object_or_404(coffret, id=coffret_id)
        print(quizz_id.quizz.id)
        question_instance = question.objects.filter(quizz=quizz_id.quizz.id)
        print(" voici les questions : ",question_instance)
        res = responsequizz.objects.filter(question__in=question_instance).all()
        print(" voici les reponses associées à toutes les questions :",res)
        if invitation_instance.status =="Accepted":
            serializer = ResponseQuizzSerializer(res, data=request.data, partial=True)
            print("serializer : ",serializer)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        return Response({"res":"invitation non accepté"})
    

class UpdateViews(APIView):
    """
    API view to update a list of MyModel objects.
    """
    serializer_class = ResponseQuizzSerializer

    def put(self, request, *args, **kwargs):
        # Get the list of objects to update from the request data
        my_model_data = request.data

        # Iterate over the list of data and update the objects
        for data in my_model_data:
            try:
                my_model = responsequizz.objects.get(pk=data['id'])
                serializer = self.serializer_class(my_model, data=data)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except responsequizz.DoesNotExist:
                return Response({"detail": "Object not found."}, status=status.HTTP_404_NOT_FOUND)

        return Response({"detail": "Objects updated successfully."}, status=status.HTTP_200_OK)
class UpdateView(APIView):

    # add permission to check if planner is authenticated
    serializer_class = ResponseQuizzSerializer
    permission_classes = [permissions.IsAuthenticated or permissions.AllowAny]

    def put(self, request):
        for obj_data in request.data:
            print(obj_data)
            print(obj_data)
            obj_id = obj_data.get('id')
            try:
                obj = responsequizz.objects.get(id=obj_id)
            except responsequizz.DoesNotExist:
                return Response({'error': f'Object with id {obj_id} does not exist'}, status=status.HTTP_404_NOT_FOUND)
            serializer = ResponseQuizzSerializer(obj, data=obj_data, partial=True)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'Objects updated successfully'})

    
class MyModelListUpdateAPIView(generics.ListCreateAPIView):

    serializer_class = ResponseQuizzSerializer

    def get_queryset(self, *args, **kwargs):
        coffret_id = self.kwargs.get('coffret_id')
        quizz_id = get_object_or_404(coffret, id=coffret_id)
        question_instance = question.objects.filter(quizz=quizz_id.quizz.id)
        res = responsequizz.objects.filter(question__in=question_instance).all()
        return res

    def put(self, request, *args, **kwargs):
        # Get the list of objects to be updated
        queryset = self.get_queryset()

        # Get the list of objects from the request data
        data = request.data
        print(data)

        # Iterate over each object in the request data
        for item_data in data:
            # Get the object to be updated
            print("Je suis dans le meme cas : ",item_data['id'])
            item = queryset.get(id=item_data['id'])
            print("Je suis dans le meme cas : ",item)
            # Update the object's fields with the new values
            item.response = item_data['response']

            # Save the updated object
            item.save()

        # Return the updated objects in the response
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class DemoViewset(generics.ListCreateAPIView):

    serializer_class = ResponseQuizzSerializer

    def get_queryset(self):
        res = responsequizz.objects.all()
        return res
    
    # Todo reécrire la methode get
    
    def post(self,request, coffret_id):

        quizz_id = get_object_or_404(coffret, id=coffret_id)
        question_instance = question.objects.filter(quizz=quizz_id.quizz.id)
        res = self.get_queryset()
        new = res.filter(question__in=question_instance).all()
        records = request.data
        print(records)
        # Let's define two lists: 
        # - one to hold the values that we want to insert,
        # - and one to hold the new values alongside existing primary keys to update
        records_to_create = []
        records_to_update = []
        
        # This is where we check if the records are pre-existing,
				# and add primary keys to the objects if they do
        records = [
            {
                "id": new.filter(
                    id=record.get("id"),
                    edited_by=record.get("edited_by"),
                    question=record.get("question"),
                    response=record.get("response")
                )
                .first()
                .id
                if new.filter(
                    id=record.get("id"),
                    edited_by=record.get("edited_by"),
                    question=record.get("question"),
                    response=record.get("response")
                ).first()
                is not None
                else None,
                **record,
            }
            for record in records
        ]
        
        # This is where we delegate our records to our split lists: 
        # - if the record already exists in the DB (the 'id' primary key), add it to the update list.
        # - Otherwise, add it to the create list.
        [
            records_to_update.append(record)
            if record["id"] is not None
            else records_to_create.append(record)
            for record in records
        ]
        
        # Remove the 'id' field, as these will all hold a value of None,
				# since these records do not already exist in the DB
        [record.pop("id") for record in records_to_create]
        
        
        created_records = responsequizz.objects.bulk_create(
            [responsequizz(**values) for values in records_to_create], batch_size=100
        )
        
        # Unless you're using Django 4.0, bulk_update doesn't return a value
        responsequizz.objects.bulk_update(
            [
                responsequizz(id=values.get("id"), response=values.get("response"))
                for values in records_to_update
            ],
            ["response"],
            batch_size=1000
        )
        
        # We may want to return different statuses and content based on the type of operations we ended up doing.
        message = None
        if len(records_to_update) > 0 and len(records_to_create) > 0:
            http_status = status.HTTP_200_OK
        elif len(records_to_update) > 0 and len(records_to_create) == 0:
            http_status = status.HTTP_204_NO_CONTENT
        elif len(records_to_update) == 0 and len(records_to_create) > 0:
            http_status = status.HTTP_201_CREATED
            message = ResponseQuizzSerializer(created_records, many=True).data
            
        return Response(message, status=http_status)
    


# url : coffret_id/participant_id/score
# parameter :  coffret_id, participant_id 
# score = 0 
# response_list1 = coffret.quizz.response.all() : list
# response_list2 = Responsequizz.question(coffret.quizz).response : list
# if coffret.quizz.question.response == Responsequizz.question(coffret.quizz).response

"""
score = 0
l1 = [1,2,3]
l2 = [3,2,5]
for i in l1:
    for j in l2:
        if(i==j):
            score = score+1
            print(score)
            break

moyenne = (score/len(l2))*100
ajouter la moyenne via un signal dans l'objet etc. 
la première fois qu'on fini le test un signal est envoyé , la prochaine fois on fait des update

if moyenne >= 50:
    print(" Bien joué voici votre récompense")
else:
    print("ça marche retente la prochaine fois !")

rendu en Json
fonction 
"""
