from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from .models import responsequizz
from planner.models import questionanswer
from django.contrib.auth.models import User

@receiver(post_save, sender=questionanswer)
def create_responsequizz(sender, instance, created, **kwargs):
    """
    Create Response quizz linked by question
    """

    if created:
        response = responsequizz.objects.create(edited_by=instance.quizz.created_by, questionanswer=instance)
        print(response)

@receiver(pre_delete, sender=questionanswer)
def delete_responsequizz(sender, instance, **kwargs):
    """
    Supprime les ResponseQuizz liées à une Question supprimée
    """
    responsequizz.objects.filter(questionanswer=instance).delete()
