from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth import get_user_model
from planner.models import questionanswer
User=get_user_model()
# Create your models here.

class responsequizz(models.Model):
    edited_by = models.ForeignKey(User, on_delete=models.CASCADE)
    questionanswer = models.ForeignKey(questionanswer, on_delete=models.CASCADE)
    response = models.CharField(max_length=250)

    def __str__(self):
        return f'{self.id}'
    